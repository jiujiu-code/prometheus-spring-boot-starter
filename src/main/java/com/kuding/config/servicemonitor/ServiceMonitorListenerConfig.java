package com.kuding.config.servicemonitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kuding.config.annos.ConditionalOnServiceMonitor;
import com.kuding.microservice.control.ServiceCheckControl;
import com.kuding.microservice.events.ServiceDiscoveredListener;
import com.kuding.microservice.events.ServiceInstanceLackEventListener;
import com.kuding.microservice.events.ServiceInstanceUnhealthyEventListener;
import com.kuding.microservice.events.ServiceLostEventListener;
import com.kuding.microservice.interfaces.ServiceNoticeRepository;
import com.kuding.properties.servicemonitor.ServiceMonitorProperties;

@Configuration
@ConditionalOnServiceMonitor
public class ServiceMonitorListenerConfig {

	@Autowired
	private ServiceMonitorProperties serviceMonitorProperties;

	@Bean
	public ServiceDiscoveredListener serviceExistedListener(ServiceCheckControl serviceCheckControl,
			DiscoveryClient discoveryClient, ApplicationEventPublisher publisher) {
		ServiceDiscoveredListener existedListener = new ServiceDiscoveredListener(serviceCheckControl,
				serviceMonitorProperties.getMonitorServices(), discoveryClient, publisher);
		return existedListener;
	}

	@Bean
	public ServiceInstanceLackEventListener serviceInstanceLackEventListener(
			ServiceNoticeRepository serviceNoticeRepository) {
		return new ServiceInstanceLackEventListener(serviceNoticeRepository);
	}

	@Bean
	public ServiceInstanceUnhealthyEventListener serviceInstanceUnhealthyEventListener(
			ServiceNoticeRepository serviceNoticeRepository) {
		return new ServiceInstanceUnhealthyEventListener(serviceNoticeRepository);
	}

	@Bean
	public ServiceLostEventListener serviceLostEventListener(ServiceNoticeRepository serviceNoticeRepository) {
		ServiceLostEventListener serviceLostEventListener = new ServiceLostEventListener(serviceNoticeRepository);
		return serviceLostEventListener;
	}
}
